from string import ascii_lowercase

mappings = {}

for i, l in enumerate(ascii_lowercase, start=1):
    mappings[str(i)] = l

def evaluator():
    l1 = "11 8 1 14 2 1 14 11"
    l2 = "19 1 25 1 8 14 1 1 19"
    l3 = "11 8 5 14 9 9"
    l4 = "14 5 18 9 9 7"
    l5 = "1 19 8 9 7 12 1 1 4"
    l6 = "2 7 1 1"
    l7 = "22 5"
    l8 = "23 15 18 11 4 9 18 / 18 15 15 20 /"
    lines = [l1, l2, l3, l4, l5, l6, l7, l8]

    for line in lines:
        line = line.split(' ')
        print()
        for l in line:
            print(mappings[l] if mappings.get(l) else "", sep="", end="")


if __name__ == "__main__":
    evaluator()
    print(mappings)
